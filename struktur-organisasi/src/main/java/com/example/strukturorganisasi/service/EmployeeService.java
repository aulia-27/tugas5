package com.example.strukturorganisasi.service;

import com.example.strukturorganisasi.model.Employee;
import com.example.strukturorganisasi.repository.EmployeeRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    JdbcTemplate jdbc;

    public List<Employee> listAll(){
        return employeeRepository.findAll();
    }

    public List<Employee> listOutput(){
        List<Employee> employees = jdbc.query(
                "SELECT e.id, e.nama, if(em.nama=e.nama,'CEO', em.nama) as atasan, c.nama from employee e " +
                        "join employee em join company c on e.company_id = c.id " +
                        "where e.atasan_id = em.id or e.atasan_id is null group by e.id" ,
                (rs, rowNum) -> new Employee(rs.getInt("e.id"), rs.getString("e.nama"), rs.getString("atasan"),rs.getString("c.nama")));
        return employees;
    }

    public void save(Employee employee){
        employeeRepository.save(employee);
    }

    public Employee get(int id){
        return employeeRepository.findById(id).get();
    }

    public void delete(int id){
        employeeRepository.deleteById(id);
    }

    public void exportReport(String reportFormat) throws FileNotFoundException, JRException {
        String pathFilePdf = "File/Report.pdf";
        String pathFileXls = "File/Report.xlsx";

        List<Employee> employeeList = listOutput();

        File file = ResourceUtils.getFile("classpath:templates/Report.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employeeList);
        Map<String, Object> params = new HashMap<>();
        params.put("Create By", "User");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
        if(reportFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint, pathFilePdf);
        }
        else if(reportFormat.equalsIgnoreCase("xlsx")){
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathFileXls));
            exporter.exportReport();
        }
    }


}
