package com.example.strukturorganisasi.service;

import com.example.strukturorganisasi.model.Company;
import com.example.strukturorganisasi.model.Employee;
import com.example.strukturorganisasi.repository.CompanyRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> listAll(){
        return companyRepository.findAll();
    }


    public void save(Company company){
        companyRepository.save(company);
    }

    public Company get(int id){
        return companyRepository.findById(id).get();
    }

    public void delete(int id){
        companyRepository.deleteById(id);
    }

    public void exportReport(String reportFormat) throws FileNotFoundException, JRException {
        String pathFilePdf = "File/ReportCompany.pdf";
        String pathFileXls = "File/ReportCompany.xlsx";

        List<Company> companies = listAll();

        File file = ResourceUtils.getFile("classpath:templates/ReportCompany.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(companies);
        Map<String, Object> params = new HashMap<>();
        params.put("Create By", "User");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
        if(reportFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint, pathFilePdf);
        }
        else if(reportFormat.equalsIgnoreCase("xlsx")){
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathFileXls));
            exporter.exportReport();
        }
    }
}
