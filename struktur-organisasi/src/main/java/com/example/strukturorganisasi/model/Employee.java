package com.example.strukturorganisasi.model;

import javax.persistence.*;

@Entity
public class Employee {
    private int id;
    private String nama;
    private String atasan_id;
    private String company_id;

    public Employee() {
    }

    public Employee(int id, String nama, String atasan_id, String company_id) {
        this.id = id;
        this.nama = nama;
        this.atasan_id = atasan_id;
        this.company_id = company_id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAtasan_id() {
        return atasan_id;
    }

    public void setAtasan_id(String atasan_id) {
        this.atasan_id = atasan_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }
}
