package com.example.strukturorganisasi.controller;

import com.example.strukturorganisasi.model.Company;
import com.example.strukturorganisasi.model.Employee;
import com.example.strukturorganisasi.service.CompanyService;
import com.example.strukturorganisasi.service.EmployeeService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;
import java.util.List;

@Controller
public class StrukturOrganisasiController {
    @Autowired
    private EmployeeService service;

    @Autowired
    private CompanyService companyService;

    @RequestMapping("/")
    public String getViewIndex(){
        return "index";
    }

    @RequestMapping("/employee")
    public String getViewPageEmployee(Model model){
        List<Employee> employees = service.listOutput();
        model.addAttribute("listEmployee", employees);
        return "employee";
    }

    @RequestMapping("/company")
    public String getViewPageCompany(Model model){
        List<Company> company = companyService.listAll();
        model.addAttribute("listCompany", company);
        return "company";
    }


    @RequestMapping("/employee/create")
    public String getCreatePageEmployee(Model model){
        Employee employee = new Employee();
        model.addAttribute(employee);
        return "employee_create";
    }

    @RequestMapping("/company/create")
    public String getCreatePageCompany(Model model){
        Company company = new Company();
        model.addAttribute(company);
        return "company_create";
    }

    @RequestMapping(value = "/employee/saveEmployee", method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute("employee") Employee employee){
        service.save(employee);
        return ("redirect:/employee");
    }

    @RequestMapping(value = "/company/saveCompany", method = RequestMethod.POST)
    public String saveCompany(@ModelAttribute("company") Company company){
        companyService.save(company);
        return ("redirect:/company");
    }

    @RequestMapping("/employee/edit/{id}")
    public ModelAndView viewEditEmployee(@PathVariable(name = "id") int id){
        ModelAndView modelAndView = new ModelAndView("employee_update");
        Employee employee = service.get(id);
        modelAndView.addObject("employee", employee);
        return modelAndView;
    }

    @RequestMapping("/company/edit/{id}")
    public ModelAndView viewEditCompany(@PathVariable(name = "id") int id){
        ModelAndView modelAndView = new ModelAndView("company_update");
        Company company = companyService.get(id);
        modelAndView.addObject("company", company);
        return modelAndView;
    }

    @RequestMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable(name = "id") int id){
        service.delete(id);
        return ("redirect:/employee");
    }

    @RequestMapping("/company/delete/{id}")
    public String deleteCompany(@PathVariable(name = "id") int id){
        companyService.delete(id);
        return ("redirect:/company");
    }

    @RequestMapping("/employee/reportPDF")
    public String ReportsPdf() throws FileNotFoundException, JRException{
        service.exportReport("pdf");
        return "redirect:/employee";
    }

    @RequestMapping("/employee/reportExcel")
    public String ReportsXls() throws FileNotFoundException, JRException{
        service.exportReport("xlsx");
        return "redirect:/employee";
    }

    @RequestMapping("/company/reportPDFCompany")
    public String ReportsPdfCompany() throws FileNotFoundException, JRException{
        companyService.exportReport("pdf");
        return "redirect:/company";
    }

    @RequestMapping("/company/reportExcelCompany")
    public String ReportsXlsCompany() throws FileNotFoundException, JRException{
        companyService.exportReport("xlsx");
        return "redirect:/company";
    }


}
