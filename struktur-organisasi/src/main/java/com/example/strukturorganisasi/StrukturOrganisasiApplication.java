package com.example.strukturorganisasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrukturOrganisasiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StrukturOrganisasiApplication.class, args);
    }

}
