package com.example.strukturorganisasi.repository;

import com.example.strukturorganisasi.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;



public interface CompanyRepository extends JpaRepository<Company, Integer> {
}
